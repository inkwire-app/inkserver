FROM python:3.9.6-buster

COPY ./ /app
WORKDIR /app
RUN pip3 install -U pip setuptools wheel
RUN pip3 install -r requirements.txt
RUN python -m spacy download en_core_web_sm

CMD sh -c "gunicorn inkserver.wsgi:app 0.0.0.0:$PORT"
