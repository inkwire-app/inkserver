import setuptools
from inkserver.version import VERSION

setuptools.setup(
    version=VERSION
)