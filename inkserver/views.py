import flask
import spacy
import regex
import pytextrank
from fuzzysearch import find_near_matches
from spacy import displacy
from inkserver.version import VERSION
from flask_restful import Resource, Api
from nltk import sent_tokenize, word_tokenize, PorterStemmer
from inkserver.models import Project, Annotation

api = flask.Blueprint("api", __name__)

nlp = spacy.load("en_core_web_sm")
nlp.add_pipe("textrank")

@api.route("/", methods=["GET"])
def root():
    """
    Return 200 OK status with some server info.
    """
    return f"inkserver {VERSION}"

@api.route("/entities", methods=["POST"])
def entities():
    data = flask.request.get_json()
    if not data:
        return flask.make_response(flask.jsonify({ "message": "Data is empty or not JSON format" }), 400)
    doc = nlp(data["doc"])
    entities_json = []
    for ent in doc.ents:
        entities_json.append({
            "text": ent.text,
            "sent": ent.root.sent.text,
            "label": ent.label_,
            "start_char": ent.start_char,
            "end_char": ent.end_char
        })
    return flask.jsonify(entities_json)

@api.route("/entities_html", methods=["POST"])
def entities_html():
    data = flask.request.get_json()
    if not data:
        return flask.jsonify({ "message": "Data is empty or not JSON format" }), 400
    doc = nlp(data["doc"])
    return displacy.render(doc, style="ent")

@api.route("/autoannotation", methods=["POST"])
def autoannotation():
    data = flask.request.get_json()
    if not data:
        return flask.jsonify({ "message": "Data is empty or not JSON format" }), 400
    doc = nlp(data["doc"])

    tr = doc._.textrank
    sentences = []
    sent_limit = data["sent_limit"]
    if not sent_limit:
        sent_limit = len(list(doc.sents)) // 5
    for sent in tr.summary(limit_phrases=sent_limit * 3, limit_sentences=sent_limit):
        sentences.append({
            "text": sent.text,
            "start_char": sent.start_char,
            "end_char": sent.end_char
        })
    return flask.jsonify(sentences)
