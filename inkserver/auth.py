import base64
import flask
import uuid
from flask import Blueprint, request
from flask_pyjwt import AuthManager, current_token, require_token
from werkzeug.security import generate_password_hash, check_password_hash
from inkserver.models import User, db

auth_api = Blueprint('auth_api', __name__)
auth_manager = AuthManager()


@auth_api.route('/login', methods=["POST"])
def login():
    name = request.get_json().get('name')
    password = request.get_json().get('password')

    user = User.query.filter_by(name=name).first()

    if not user or not check_password_hash(user.password, password):
        return flask.make_response(flask.jsonify({
            "message": "User or password invalid."
        }), 401)
    auth_token = auth_manager.auth_token(user.id)
    refresh_token = auth_manager.refresh_token(user.id)
    return {
        "auth_token": auth_token.signed,
        "refresh_token": refresh_token.signed
    }, 200

@auth_api.route('/login/refresh', methods=["POST"])
@require_token("refresh")
def refresh():
    auth_token = auth_manager.auth_token(current_token.sub)
    refresh_token = auth_manager.refresh_token(current_token.sub)
    return {
        "auth_token": auth_token.signed,
        "refresh_token": refresh_token.signed
    }, 200

@auth_api.route('/signup', methods=["POST"])
def signup():
    name = request.get_json().get('name')
    password = request.get_json().get('password')

    user = User.query.filter_by(name=name).first()
    if user:
        return flask.make_response(flask.jsonify({
            "message": "User already exists. Login instead."
        }), 401)

    new_user = User(name=name,
                    password=generate_password_hash(password, method='sha256'))

    # add the new user to the database
    db.session.add(new_user)
    db.session.commit()

    return flask.make_response(flask.jsonify({}), 200)
