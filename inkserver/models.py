"""
Some example SQLAlchemy models.

In this case, we define a person model
with a one-to-many relationship to a thing model.

One person can have multiple things, and a thing has one person
associated with it.
"""
from flask_login import UserMixin
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True)
    password = db.Column(db.String(80))

    projects = db.relationship("Project", backref="user", lazy=True, cascade="all, delete-orphan")
    sources = db.relationship("Source", backref="user", lazy=True, cascade="all, delete-orphan")
    annotations = db.relationship("Annotation", backref="user", lazy=True, cascade="all, delete-orphan")
    papers = db.relationship("Paper", backref="user", lazy=True, cascade="all, delete-orphan")

class Project(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    citation_style = db.Column(db.String(80))
    last_edited = db.Column(db.String(255), nullable=True)

    sources = db.relationship("Source", backref="project", lazy=True, cascade="all, delete-orphan")
    annotations = db.relationship("Annotation", backref="project", lazy=True, cascade="all, delete-orphan")

    user_id = db.Column(db.Integer, db.ForeignKey("user.id"),
        nullable=False)
    paper = db.relationship("Paper", backref="project", lazy=True, cascade="all, delete-orphan", uselist=False)


class Source(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    source_type = db.Column(db.String(100))

    authors = db.relationship("Author", backref="source", lazy=True, cascade="all, delete-orphan")

    title = db.Column(db.String(255))
    url = db.Column(db.String(1024), nullable=True)
    access_date = db.Column(db.String(255), nullable=True)
    edition = db.Column(db.String(64), nullable=True)
    volume = db.Column(db.String(64), nullable=True)

    publisher = db.Column(db.String(255), nullable=True)
    publisher_location = db.Column(db.String(512), nullable=True)
    publish_date = db.Column(db.String(255), nullable=True)

    text = db.Column(db.Text, nullable=True)
    citation = db.Column(db.Text, nullable=True)
    last_edited = db.Column(db.String(255), nullable=True)

    annotations = db.relationship("Annotation", backref="source", lazy=True, cascade="all, delete-orphan")

    project_id = db.Column(db.Integer, db.ForeignKey("project.id"),
        nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"),
        nullable=False)

class Author(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(64))
    first = db.Column(db.String(128))
    last = db.Column(db.String(128))
    full = db.Column(db.String(128))
    source_id = db.Column(db.Integer, db.ForeignKey("source.id"),
        nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"),
        nullable=False)

class Annotation(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(80))
    start_char = db.Column(db.Integer, nullable=True)
    end_char = db.Column(db.Integer, nullable=True)

    source_id = db.Column(db.Integer, db.ForeignKey("source.id"),
        nullable=False)
    project_id = db.Column(db.Integer, db.ForeignKey("project.id"),
        nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"),
        nullable=False)

class Paper(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    text = db.Column(db.Text, default="")

    project_id = db.Column(db.Integer, db.ForeignKey("project.id"),
        nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"),
        nullable=False)
