import flask
import uuid
from flask_pyjwt import current_token, require_token
from flask_restful import Resource, Api
from marshmallow.exceptions import ValidationError
from inkserver.models import Project, Source, Author, Annotation, Paper, db
from inkserver.schemas import ProjectSchema, SourceSchema, AnnotationSchema, AuthorSchema, PaperSchema

model_api = flask.Blueprint("model", __name__)
rest_api = Api(model_api)

class ProjectView(Resource):
    @require_token()
    def get(self, id=None):
        print("get")
        if id is None:
            model = Project.query.filter_by(user_id=current_token.sub)
            data = ProjectSchema(many=True).dump(model)
        else:
            model = Project.query.filter_by(user_id=current_token.sub).filter_by(id=id)
            if model.first() is None:
                return flask.make_response(flask.jsonify({
                    "message": "Specified object does not exist"
                }), 404)
            data = ProjectSchema().dump(model.first())
        return data

    @require_token()
    def post(self, id=None):
        try:
            if id is None:
                data = ProjectSchema().load({
                    **flask.request.get_json(),
                    "user_id": current_token.sub
                })
                model = Project(**data)
                db.session.add(model)
                db.session.commit()
                return ProjectSchema().dump(model)
            else:
                model = Project.query.filter_by(user_id=current_token.sub).filter_by(id=id)
                if model.first() is None:
                    print("haha")
                    return flask.make_response(flask.jsonify({
                        "message": "Specified object does not exist"
                    }), 404)
                data = { **flask.request.get_json(), "user_id": current_token.sub }
                data = ProjectSchema().load(data, partial=True)
                model.update(data)
                db.session.commit()
                return ProjectSchema().dump(model.first())
        except ValidationError as e:
            return flask.make_response(flask.jsonify(str(e)), 400)

    @require_token()
    def put(self, id=None):
        return self.post(id)

    @require_token()
    def delete(self, id=None):
        model = Project.query.filter_by(user_id=current_token.sub).filter_by(id=id).first()
        if model is None:
            return flask.make_response(flask.jsonify({
                "message": "Specified object does not exist"
            }), 404)
        else:
            db.session.delete(model)
            db.session.commit()
            return flask.jsonify({})

@model_api.route("/project/<project_id>/annotations", methods=["GET"])
def project_annotations(project_id: int):
    project_model = Project.query.filter_by(user_id=current_token.sub).filter_by(id=project_id)
    if project_model.first() is None:
        return flask.make_response(flask.jsonify({
            "message": "Specified object does not exist"
        }), 404)
    annotations = Annotation.query.filter_by(user_id=current_token.sub).filter_by(project_id=project_id)
    return flask.jsonify(AnnotationSchema(many=True).dump(annotations))


class SourceView(Resource):
    @require_token()
    def get(self, id=None):
        if id is None:
            model = Source.query.filter_by(user_id=current_token.sub)
            data = SourceSchema(many=True).dump(model)
        else:
            model = Source.query.filter_by(user_id=current_token.sub).filter_by(id=id)
            if model.first() is None:
                return flask.make_response(flask.jsonify({
                    "message": "Specified object does not exist"
                }), 404)
            data = SourceSchema().dump(model.first())
        return data

    @require_token()
    def post(self, id=None):
        try:
            if id is None:
                data = SourceSchema().load({
                    **flask.request.get_json(),
                    "user_id": current_token.sub
                })
                model = Source(**data)
                db.session.add(model)
                db.session.commit()
                return SourceSchema().dump(model)
            else:
                data = { **flask.request.get_json(), "user_id": current_token.sub }
                data = SourceSchema().load(data, partial=True)
                model = Source.query.filter_by(user_id=current_token.sub).filter_by(id=id)
                if model.first() is None:
                    return flask.make_response(flask.jsonify({
                        "message": "Specified object does not exist"
                    }), 404)
                model.update(data)
                db.session.commit()
                return SourceSchema().dump(model.first())
        except ValidationError as e:
            return flask.make_response(flask.jsonify(str(e)), 400)

    @require_token()
    def put(self, id=None):
        return self.post(id)

    @require_token()
    def delete(self, id=None):
        model = Source.query.filter_by(user_id=current_token.sub).filter_by(id=id)
        if model.first() is None:
            return flask.make_response(flask.jsonify({
                "message": "Specified object does not exist"
            }), 404)
        else:
            db.session.delete(model.first())
            db.session.commit()
            return flask.jsonify({})


class AuthorView(Resource):
    @require_token()
    def get(self, id=None):
        if id is None:
            model = Author.query.filter_by(user_id=current_token.sub)
            data = AuthorSchema(many=True).dump(model)
        else:
            model = Author.query.filter_by(user_id=current_token.sub).filter_by(id=id)
            if model.first() is None:
                return flask.make_response(flask.jsonify({
                    "message": "Specified object does not exist"
                }), 404)
            data = AuthorSchema().dump(model.first())
        return data

    @require_token()
    def post(self, id=None):
        try:
            if id is None:
                data = AuthorSchema().load({
                    **flask.request.get_json(),
                    "user_id": current_token.sub
                })
                model = Author(**data)
                db.session.add(model)
                db.session.commit()
                return AuthorSchema().dump(model)
            else:
                data = { **flask.request.get_json(), "user_id": current_token.sub }
                data = AuthorSchema().load(data, partial=True)
                model = Author.query.filter_by(user_id=current_token.sub).filter_by(id=id)
                if model.first() is None:
                    return flask.make_response(flask.jsonify({
                        "message": "Specified object does not exist"
                    }), 404)
                model.update(data)
                db.session.commit()
                return AuthorSchema().dump(model.first())
        except ValidationError as e:
            return flask.make_response(flask.jsonify(str(e)), 400)

    @require_token()
    def put(self, id=None):
        return self.post(id)

    @require_token()
    def delete(self, id=None):
        model = Author.query.filter_by(user_id=current_token.sub).filter_by(id=id)
        if model.first() is None:
            return flask.make_response(flask.jsonify({
                "message": "Specified object does not exist"
            }), 404)
        else:
            db.session.delete(model.first())
            db.session.commit()
            return flask.jsonify({})


class AnnotationView(Resource):
    @require_token()
    def get(self, id=None):
        print("get")
        if id is None:
            model = Annotation.query.filter_by(user_id=current_token.sub)
            data = AnnotationSchema(many=True).dump(model)
        else:
            model = Annotation.query.filter_by(user_id=current_token.sub).filter_by(id=id)
            if model.first() is None:
                return flask.make_response(flask.jsonify({
                    "message": "Specified object does not exist"
                }), 404)
            data = AnnotationSchema().dump(model.first())
        return data

    @require_token()
    def post(self, id=None):
        try:
            if id is None:
                data = AnnotationSchema().load({
                    **flask.request.get_json(),
                    "user_id": current_token.sub
                })
                model = Annotation(**data)
                db.session.add(model)
                db.session.commit()
                return AnnotationSchema().dump(model)
            else:
                data = { **flask.request.get_json(), "user_id": current_token.sub }
                data = AnnotationSchema().load(data, partial=True)
                model = Annotation.query.filter_by(user_id=current_token.sub).filter_by(id=id)
                if model.first() is None:
                    return flask.make_response(flask.jsonify({
                        "message": "Specified object does not exist"
                    }), 404)
                else:
                    model.update(data)
                    db.session.commit()
                    return AnnotationSchema().dump(model.first())
        except ValidationError as e:
            return flask.make_response(flask.jsonify(str(e)), 400)

    @require_token()
    def put(self, id=None):
        return self.post(id)

    @require_token()
    def delete(self, id=None):
        model = Annotation.query.filter_by(user_id=current_token.sub).filter_by(id=id)
        if model.first() is None:
            return flask.make_response(flask.jsonify({
                "message": "Specified object does not exist"
            }), 404)
        else:
            db.session.delete(model.first())
            db.session.commit()
            return flask.jsonify({})


class PaperView(Resource):
    @require_token()
    def get(self, id=None):
        print("get")
        if id is None:
            model = Paper.query.filter_by(user_id=current_token.sub)
            data = PaperView(many=True).dump(model)
        else:
            model = Paper.query.filter_by(user_id=current_token.sub).filter_by(id=id)
            if model.first() is None:
                return flask.make_response(flask.jsonify({
                    "message": "Specified object does not exist"
                }), 404)
            data = PaperSchema().dump(model.first())
        return data

    @require_token()
    def post(self, id=None):
        try:
            if id is None:
                data = PaperSchema().load({
                    **flask.request.get_json(),
                    "user_id": current_token.sub
                })
                model = Paper(**data)
                db.session.add(model)
                db.session.commit()
                return PaperSchema().dump(model)
            else:
                data = { **flask.request.get_json(), "user_id": current_token.sub }
                data = PaperSchema().load(data, partial=True)
                model = Paper.query.filter_by(user_id=current_token.sub).filter_by(id=id)
                if model.first() is None:
                    return flask.make_response(flask.jsonify({
                        "message": "Specified object does not exist"
                    }), 404)
                else:
                    model.update(data)
                    db.session.commit()
                    return PaperSchema().dump(model.first())
        except ValidationError as e:
            return flask.make_response(flask.jsonify(str(e)), 400)

    @require_token()
    def put(self, id=None):
        return self.post(id)

    @require_token()
    def delete(self, id=None):
        model = Paper.query.filter_by(user_id=current_token.sub).filter_by(id=id)
        if model.first() is None:
            return flask.make_response(flask.jsonify({
                "message": "Specified object does not exist"
            }), 404)
        else:
            db.session.delete(model.first())
            db.session.commit()
            return flask.jsonify({})


rest_api.add_resource(ProjectView, "/project", "/project/<id>")
rest_api.add_resource(SourceView, "/source", "/source/<id>")
rest_api.add_resource(AuthorView, "/author", "/author/<id>")
rest_api.add_resource(AnnotationView, "/annotation", "/annotation/<id>")
rest_api.add_resource(PaperView, "/paper", "/paper/<id>")
