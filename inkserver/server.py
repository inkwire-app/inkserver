import os
import flask
from flask_cors import CORS
from inkserver.settings import Settings
from inkserver.views import api
from inkserver.version import VERSION
from inkserver.models import db
from inkserver.schemas import ma
from inkserver.auth import auth_manager
from inkserver.model_views import model_api
from inkserver.auth import auth_api

app = flask.Flask(__name__)
app.config["MAX_CONTENT_LENGTH"] = Settings.UPLOAD_SIZE_MAX
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///inkserver.db'
app.config['SECRET_KEY'] = os.environ.get("SECRET", "joinamadoruavswearethebestclub")
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_ECHO'] = True
app.config["JWT_ISSUER"] = "Flask_PyJWT"
app.config["JWT_AUTHTYPE"] = "HS256"
app.config["JWT_SECRET"] = os.environ.get("SECRET", "joinamadoruavswearethebestclub")

db.init_app(app)
auth_manager.init_app(app)
with app.app_context():
    db.create_all()

CORS(app)

@app.route("/")
def root():
    return f"inkserver version {VERSION}"

app.register_blueprint(api, url_prefix="/api")
app.register_blueprint(model_api, url_prefix="/api")
app.register_blueprint(auth_api, url_prefix="/api")

if __name__ == "__main__":
    app.run(port=5000) # Dev server
