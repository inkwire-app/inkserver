"""
Marshmallow schemas.

These schemas intelligently handle automatically
serializing the models into usable datatypes.
"""
from datetime import time
from marshmallow import fields, pprint
from flask_marshmallow import Marshmallow
from inkserver.models import Project, Source, Annotation, Author, Paper, db

ma = Marshmallow()

class ProjectSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Project
        sqla_session = db.session
        include_fk = True

    sources = ma.List(ma.Nested("SourceSchema"))
    annotations = ma.List(ma.Nested("AnnotationSchema"))
    paper = ma.Nested("PaperSchema")

class SourceSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Source
        sqla_session = db.session
        include_fk = True

    authors = ma.List(ma.Nested("AuthorSchema"))
    annotations = ma.List(ma.Nested("AnnotationSchema"))

class AuthorSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Author
        sqla_session = db.session
        include_fk = True

class AnnotationSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Annotation
        sqla_session = db.session
        include_fk = True

class PaperSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Paper
        sqla_session = db.session
        include_fk = True
